#include <stdlib.h>
#include <X11/Xlib.h>
#include <time.h>
#include <stdbool.h>
#include <zconf.h>
#include <string.h>
#include <stdio.h>

bool debugEnabled = false;

unsigned int getRandom(int lower, int upper) {
    unsigned int seed = (unsigned int) time(NULL);
    srand(seed);
    return (unsigned int) (rand() % (upper - lower + 1) + lower);
}

void moveCursor() {
    Display *dpy;
    Window root_window;

    dpy = XOpenDisplay(0);
    Screen *screen = XScreenOfDisplay(dpy, 0);
    root_window = XRootWindow(dpy, 0);
    unsigned int width = (unsigned int) XWidthOfScreen(screen);
    unsigned int height = (unsigned int) XHeightOfScreen(screen);

    int destX = getRandom(0, width);
    int destY = getRandom(0, height);

    XWarpPointer(dpy, None, root_window, 0, 0, width, height, destX, destY);
    XFlush(dpy);

    if (debugEnabled) {
        printf("Moved cursor to x: %d y: %d.\n", destX, destY);
    }
}

int main(int argc, char **argv) {
    int minSleep = 60;
    int maxSleep = 600;

    if(argc > 1) {
        for (int i = 1; i <= (argc - 1); ++i) {
            if (strcmp(argv[i], "--debug") == 0) {
                debugEnabled = true;
                minSleep = 1;
                maxSleep = 10;
            }
        }
    }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    while(true) {
        unsigned int sleepTime = getRandom(minSleep, maxSleep);
        if (debugEnabled) {
            printf("Sleeping for %d seconds.\n", sleepTime);
        }
        sleep(sleepTime);

        if (getRandom(0, 100) > 50) {
            continue;
        }

        moveCursor();
    }
#pragma clang diagnostic pop
}
